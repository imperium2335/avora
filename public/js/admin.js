'use strict';

$(function () {

        var saveTimeOut;
        var prevVal = '';
        $('.form-control').on('input', function () {
            var val = $(this).val();

            if(!!$(this).attr('max')) {
                if(val/1 > $(this).attr('max')) {
                    $(this).val($(this).attr('max'));
                }
                else if(val/1 < $(this).attr('min')) {
                    $(this).val($(this).attr('min'));
                }
                val = $(this).val();
            }

            var mode = $(this).hasClass('percent') ? 'percent' : 'number';

            $(this).parent().find('.input-group-addon:last-child').html(numberAlias(val, mode));
            if (val != prevVal) {
                prevVal = val;
                save();
            }
        })

        function save() {
            clearTimeout(saveTimeOut);
            saveTimeOut = setTimeout(function () {
                var form = document.getElementById('statistics_form');
                var $form = $(form);
                var formData = new FormData(form);
                qwest.post(form.action, formData)
                    .then(function (xhr, response) {
                        $('.panel-main').removeClass('panel-default');
                        $('.panel-main').addClass('panel-success').removeClass('panel-danger');
                        $('.panel-footer').html(response.msg);
                        setTimeout(function () {
                            $('.panel-main').addClass('panel-default').removeClass('panel-success');
                            $('.panel-footer').html('Everything is awesome!');
                        }, 3000)
                    })
                    .catch(function (xhr, response, e) {
                        $('.panel-main').removeClass('panel-default');
                        $('.panel-main').addClass('panel-danger').removeClass('panel-success');
                        $('.panel-footer').html(response.msg);
                    });
            }, 500)
        }

        function numberAlias(number, mode) {
            if (mode == 'number') {
                var mil = Math.pow(10, 6);
                var bil = Math.pow(10, 9);

                if (number >= bil)
                    return format("#,##0.##", (number / bil)) + ' billion';
                else if (number >= mil)
                    return format("#,##0.##", (number / mil)) + ' million';

                return format("#,##0.##", number);
            }
            else if (mode == 'percent') {
                return number + '%';
            }
        }

    }
)