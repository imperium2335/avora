'use strict';

$(function () {
    renderActiveUsersChart();
    renderShareChart();

    $('.prev-slide').on('click', function () {
        window.mySwipe.prev();
    });
    $('.next-slide').on('click', function () {
        window.mySwipe.next();
    });

    $('.blue').on('click', function () {
        $('.orange img').attr('src', 'http://avora.s3.amazonaws.com/navigation/online-share-sales.png');
        $('.blue img').attr('src', 'http://avora.s3.amazonaws.com/navigation/digitally-active-selected.png');
        window.mySwipe.slide(1, 800)
    });
    $('.orange').on('click', function () {
        $('.orange img').attr('src', 'http://avora.s3.amazonaws.com/navigation/online-share-sales-selected.png');
        $('.blue img').attr('src', 'http://avora.s3.amazonaws.com/navigation/digitally-active.png');
        window.mySwipe.slide(0, 800)
    });

    $('.pip.first').on('click', function () {
        window.mySwipe.slide(0, 800);
    });
    $('.pip.third').on('click', function () {
        window.mySwipe.slide(1, 800);
    });

    window.mySwipe = Swipe(document.getElementById('chart_area'), {
        startSlide: 0,
        speed: 800,
        continuous: false,
        disableScroll: false,
        stopPropagation: false,
        callback: function (index, elem) {
            if (index == 1) {
                $('.orange img').attr('src', 'http://avora.s3.amazonaws.com/navigation/online-share-sales.png');
                $('.blue img').attr('src', 'http://avora.s3.amazonaws.com/navigation/digitally-active-selected.png');
                $('#main_title').fadeOut('fast', function () {
                    $(this).html('DIGITALLY ACTIVE<br />CUSTOMERS &amp; LOGINS');
                    $(this).fadeIn('fast')
                });
                $('.pip').removeClass('pip-active');
                $('.pip.third').addClass('pip-active');
            } else {
                $('.orange img').attr('src', 'http://avora.s3.amazonaws.com/navigation/online-share-sales-selected.png');
                $('.blue img').attr('src', 'http://avora.s3.amazonaws.com/navigation/digitally-active.png');
                $('#main_title').fadeOut('fast', function () {
                    $(this).html('ONLINE SHARE OF SALES');
                    $(this).fadeIn('fast')
                });
                $('.pip').removeClass('pip-active');
                $('.pip.first').addClass('pip-active');
            }
        },
        transitionEnd: function (index, elem) {
        }
    });

    function renderActiveUsersChart() {
        new Chartist.Bar('.dacl-chart', {
            labels: [globalData.last_md, globalData.current_md],
            series: [
                [globalData.total_customers_last_year, globalData.total_customers_current_year],
            ]
        }, {
            seriesBarDistance: 0,
            fullWidth: true,
            axisX: {
                offset: 15
            },
            axisY: {
                offset: 20,
                labelInterpolationFnc: function (value) {
                    var bil = Math.pow(10, 9);
                    var mil = Math.pow(10, 6);
                    var k = Math.pow(10, 3);
                    if (value >= bil)
                        return value / bil + 'B';
                    else if (value >= mil)
                        return value / mil + 'M';
                    else if (value >= k)
                        return value / k + 'k';
                    else
                        return value
                },
                scaleMinSpace: 25
            }
        });
    }

    function renderShareChart() {
        var chart = new Chartist.Bar('#online_share', {
            labels: [globalData.f1, globalData.f2, globalData.f3, globalData.f4, globalData.f5],
            series: [
                [globalData.year_2, globalData.year_3, globalData.year_4, globalData.year_5, globalData.year_6],
            ]
        }, {
            seriesBarDistance: 0,
            fullWidth: true,
            axisX: {
                offset: 35,
                labelInterpolationFnc: function (value) {
                    if(value.indexOf('YTD'))
                    return value + '<br />Financial Year';
                    else return value;
                },
            },
            axisY: {
                offset: 0,
                labelInterpolationFnc: function (value) {
                    return value + '%';
                },
                scaleMinSpace: 40
            }
        })
        chart.on('draw', function(data) {
            if(data.type === 'label' && data.axis.units.pos === 'y') {
                data.element.attr({
                    x: -10
                });
            }
            var barHorizontalCenter, barVerticalCenter, label, share, value;
            if (data.type === "bar") {
                barHorizontalCenter = data.x1 + (data.element.width() * .5);
                barVerticalCenter = data.y1 + (data.element.height() * -1) - 10;

                value = data.element.attr('ct:value');
                if (value !== '0') {
                    label = new Chartist.Svg('text');
                    label.text(value + '%');
                    label.addClass("ct-barlabel");
                    label.attr({
                        x: barHorizontalCenter,
                        y: 200,
                        'text-anchor': 'middle'
                    });

                    share = new Chartist.Svg('text');
                    share.text('SHARE');
                    share.addClass("ct-barlabel-share");
                    share.attr({
                        x: barHorizontalCenter,
                        y: 215,
                        'text-anchor': 'middle'
                    });
                    data.group.append(share);
                    return data.group.append(label);
                }
            }
        });

    }

    $( window ).resize(function() {
        renderShareChart(); // Fix window resize bug.
    })

});