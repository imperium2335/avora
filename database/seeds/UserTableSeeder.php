<?PHP

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->truncate();
        \App\Models\User::insert([
                [
                    'email' => 'tom@tcwhiggins.com',
                    'password' => Hash::make('admin'),
                    'enabled' => 1
                ],
                [
                    'email' => 'mikeT@gmail.com',
                    'password' => Hash::make('admin'),
                    'enabled' => 1
                ],
                [
                    'email' => 'j.jackson@gmail.com',
                    'password' => Hash::make('admin'),
                    'enabled' => 1
                ]
            ]
        );
    }
}