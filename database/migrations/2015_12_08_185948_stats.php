<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Stats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('total_customers_last_year')->default(0);
            $table->integer('total_customers_current_year')->default(0);
            $table->integer('bankers_last_year')->default(0);
            $table->integer('bankers_this_year')->default(0);
            $table->integer('mobile_last_year')->default(0);
            $table->integer('mobile_this_year')->default(0);
            $table->integer('year_1')->default(0);
            $table->integer('year_2')->default(0);
            $table->integer('year_3')->default(0);
            $table->integer('year_4')->default(0);
            $table->integer('year_5')->default(0);
            $table->integer('year_6')->default(0);
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stats');
    }
}
