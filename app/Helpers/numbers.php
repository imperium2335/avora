<?php

function numberAlias($input, $thousands = false, $percent = false)
{
    if (!$percent) {
        $k = pow(10, 3);
        $mil = pow(10, 6);
        $bil = pow(10, 9);

        if ($input >= $bil)
            return (float)($input / $bil) . ' billion';
        else if ($input >= $mil)
            return (float)($input / $mil) . ' million';
        elseif ($thousands)
            return (float)($input / $k) . 'k';

        return number_format($input, 0);
    } else {
        $input = $input . '%';
        return $input;
    }
}

function trend($last, $now)
{
    if ($last) {
        return round(($now / $last) * 100 - 100);
    }
    return 0;
}