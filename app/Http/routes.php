<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => '', 'uses' => function () {
    $q = DB::table('stats')->first();
    if ($q) {
        $q->tc_trend = $q->total_customers_last_year > $q->total_customers_current_year ? '-' : '+';
        $q->b_trend = $q->bankers_last_year > $q->bankers_this_year ? '-' : '+';
        $q->m_trend = $q->mobile_last_year > $q->mobile_this_year ? '-' : '+';
        $q->mty_height = $q->mly_height = 1;
        if ($q->mobile_last_year >= $q->mobile_this_year) {
            $q->mty_height = $q->mobile_last_year ? ($q->mobile_this_year / $q->mobile_last_year * 94 / 100) : 94;
            if ($q->mty_height < 0.5) $q->mty_height = 0.5;
        } else {
            $q->mly_height = $q->mly_height ? ($q->mobile_last_year / $q->mobile_this_year * 94 / 100) : 94;
            if ($q->mly_height < 0.5) $q->mly_height = 0.5;
        }

        if ($q->bankers_last_year >= $q->bankers_this_year) {
            $q->red_men = $q->bankers_last_year ? (int)($q->bankers_this_year / $q->bankers_last_year * 10) : 0;
            $q->white_men = 10;
        } else {
            $q->white_men = $q->bankers_this_year ? (int)($q->bankers_last_year / $q->bankers_this_year * 10) : 0;
            $q->red_men = 10;
        }

        $now = \Carbon\Carbon::now();
        $q->current_md = date('M Y');
        $q->last_md = $now->copy()->subYear()->format('M Y');
        $q->f1 = $now->copy()->subYears(4)->format('Y') . '/' . $now->copy()->subYears(3)->format('y');
        $q->f2 = $now->copy()->subYears(3)->format('y') . '/' . $now->copy()->subYears(2)->format('y');
        $q->f3 = $now->copy()->subYears(2)->format('y') . '/' . $now->copy()->subYears(1)->format('y');
        $q->f4 = $now->copy()->subYears(1)->format('y') . '/' . $now->copy()->format('y');
        $q->f5 = 'YTD<br />' . '(' . $now->copy()->format('M Y') . ')';
    }
    return view()->make('high-flying.container', ['dacl' => $q]);
}]);

Route::get('digitally-active-stats', ['as' => 'digitally-active-stats', 'uses' => 'StatisticsController@digitallyActive']);

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');


Route::get('admin', ['as' => 'admin', 'uses' => function () {
    if (Auth::check()) {
        return redirect()->route('admin.manage');
    }
    return view()->make('admin.login');
}]);

Route::group(['middleware' => 'auth'], function () {

    Route::get('admin/manage', ['as' => 'admin.manage', 'uses' => 'AdminController@show']);
    Route::post('admin/save-stats', ['as' => 'admin.save-stats', 'uses' => 'AdminController@update']);
    Route::get('auth/logout', 'Auth\AuthController@getLogout');

});