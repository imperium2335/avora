<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class StatisticsController extends Controller
{
    public function digitallyActive() {
        $q = DB::table('stats')->first();
        $q->tc_trend = $q->total_customers_last_year > $q->total_customers_current_year ? '-' : '+';
        $q->b_trend = $q->bankers_last_year > $q->bankers_this_year ? '-' : '+';
        $q->m_trend = $q->mobile_last_year > $q->mobile_this_year ? '-' : '+';
        return $q;
    }
}
