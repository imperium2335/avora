<?php

namespace App\Http\Controllers;

use App\Models\Statistics;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function show()
    {
        $data = DB::table('stats')->first();
        return view()->make('admin.manage', ['data' => $data]);
    }

    public function update(Request $request)
    {
        // No need to check if the user is logged in here as middleware (in routes.php) takes care of this.
        $stats = Statistics::first();

        $msg = '';
        if (!$stats) { // If the row doesn't exist, create one.
            $stats = new Statistics;
            $msg .= 'The data model was missing, but was created successfully. ';
        }
        $stats->total_customers_last_year = $request->get('last_year_total_customers');
        $stats->total_customers_current_year = $request->get('this_year_total_customers');
        $stats->bankers_last_year = $request->get('last_year_bankers');
        $stats->bankers_this_year = $request->get('this_year_bankers');
        $stats->mobile_last_year = $request->get('last_year_mobile');
        $stats->mobile_this_year = $request->get('this_year_mobile');

        $stats->year_1 = $request->get('year_1');
        $stats->year_2 = $request->get('year_2');
        $stats->year_3 = $request->get('year_3');
        $stats->year_4 = $request->get('year_4');
        $stats->year_5 = $request->get('year_5');
        $stats->year_6 = $request->get('year_6');
        $msg .= 'Statistics updated successfully.';
        if ($stats->save()) return \Response::make(['msg' => $msg], 200);

    }
}
