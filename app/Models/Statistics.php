<?php
/**
 * Created by PhpStorm.
 * User: tom.higgins
 * Date: 10/12/2015
 * Time: 10:26
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Statistics extends Model
{
    protected $table = 'stats';
}