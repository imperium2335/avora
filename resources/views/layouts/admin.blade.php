<head>
    <title>Data Management - Digital Insights &amp; Analytics</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    @yield('css')
</head>
<body>
<div class="container-fluid">
    @include('includes.head')
    @yield('content')
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="/js/qwest.js"></script>
    <script type="text/javascript" src="/js/number.js"></script>
    <script type="text/javascript" src="/js/admin.js"></script>
</div>
</body>