<head>
    <title>Digital Insights &amp; Analytics</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/app.css">
    <link rel="stylesheet" type="text/css" href="css/slide-bg.css">
    <link rel="stylesheet" type="text/css" href="css/chartist.min.css">
    @yield('css')
</head>
<body>
<div class="container-fluid">
    @include('includes.head')
    @yield('content')
    @include('includes.footer')
    <script type="text/javascript">
        var globalData = <?php echo json_encode($dacl);?>;
        console.log(globalData)
    </script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="js/qwest.js"></script>
    <script type="text/javascript" src="js/swipe.js"></script>
    <script src="js/chartist.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/slides.js"></script>
    @yield('js')
</div>
</body>