@extends('layouts.master')

@section('content')
    <?php $now = \Carbon\Carbon::now();?>
    <div class="row">
        <div class="col-xs-12 slides">
            <div class="row">
                <div class="col-xs-3"><h2 class="hidden-xs">At a glance...</h2></div>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12"><h1>High Flying User Statistics</h1></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12"><h2 id="main_title">ONLINE SHARE OF SALES</h2></div>
                    </div>
                </div>
            </div>


            <img class="balloon hidden-xs" src="http://avora.s3.amazonaws.com/slides/balloon.png">
            <img class="cloud cloud-1 hidden-xs" src="http://avora.s3.amazonaws.com/slides/cloud-lg.png">
            <img class="cloud cloud-2 hidden-xs" src="http://avora.s3.amazonaws.com/slides/cloud-sm.png">
            <img class="cloud cloud-3 hidden-xs" src="http://avora.s3.amazonaws.com/slides/cloud-lg.png">
            <img class="cloud cloud-4 hidden-xs" src="http://avora.s3.amazonaws.com/slides/cloud-sm.png">
            <img class="cloud cloud-5 hidden-xs" src="http://avora.s3.amazonaws.com/slides/cloud-lg.png">
            <img class="cloud cloud-6 hidden-xs" src="http://avora.s3.amazonaws.com/slides/cloud-outline.png">
            <img class="cloud cloud-7 hidden-xs distant" src="http://avora.s3.amazonaws.com/slides/cloud-dark.png">
            <img class="cloud cloud-8 hidden-xs distant" src="http://avora.s3.amazonaws.com/slides/cloud-dark.png">
            <img class="cloud cloud-9 hidden-xs distant" src="http://avora.s3.amazonaws.com/slides/cloud-dark.png">
            <img class="cloud cloud-10 hidden-xs" src="http://avora.s3.amazonaws.com/slides/cloud-sm.png">
            <img class="cloud cloud-11 hidden-xs" src="http://avora.s3.amazonaws.com/slides/cloud-sm.png">
            <img class="slide-control prev-slide" src="http://avora.s3.amazonaws.com/slides/left.png">
            <img class="slide-control next-slide" src="http://avora.s3.amazonaws.com/slides/right.png">

            <div class="row swipe" id="chart_area">
                <div class='swipe-wrap'>
                    <div class="col-xs-12">
                        <div class="row">
                            <div id="online_share" class="col-xs-10 col-xs-offset-1"></div>
                        </div>
                    </div>
                    <div id="digitally_active" class="col-xs-12">
                        <div class="dacl-box">
                            <div class="dacl-title">TOTAL DIGITALLY ACTIVE CUSTOMERS</div>
                            <div class="gfx-container dacl-chart">

                            </div>
                            <div class="dacl-total">{{$dacl ? strtoupper(numberAlias($dacl->total_customers_current_year)) : 0}}</div>
                            <div class="dacl-total-name">TOTAL ACTIVE USERS</div>
                            <div class="trend">

                                {{$dacl && $dacl->tc_trend == '-' ? '-' : '+'}}{{$dacl ? trend($dacl->total_customers_last_year, $dacl->total_customers_current_year) : 0}}
                                % TREND <span
                                        class="glyphicon glyphicon-arrow-{{$dacl && $dacl->tc_trend == '-' ? 'down' : 'up'}}"></span>
                            </div>
                        </div>
                        <div class="dacl-box">
                            <div class="dacl-title">ACTIVE INTERNET BANKERS</div>
                            <div class="gfx-container men">
                                <div class="white-men">
                                    @if($dacl)
                                        @for($i=0;$i<$dacl->white_men;$i++)
                                            <img src="http://avora.s3.amazonaws.com/slides/man-white.png">
                                        @endfor
                                    @endif
                                    <div>
                                        <span class="pull-left">{{$now->copy()->subYear()->format('M Y')}}</span>
                                        <span class="pull-right">{{$dacl ? numberAlias($dacl->bankers_last_year) : 0}}</span>
                                    </div>
                                </div>
                                <div class="red-men">
                                    @if($dacl)
                                        @for($i=0;$i<$dacl->red_men;$i++)
                                            <img src="http://avora.s3.amazonaws.com/slides/man-red.png">
                                        @endfor
                                    @endif
                                    <div>
                                        <span class="pull-left">{{$now->format('M Y')}}</span>
                                        <span class="pull-right">{{$dacl ? numberAlias($dacl->bankers_this_year) : 0}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="dacl-total">{{$dacl ? strtoupper(numberAlias($dacl->bankers_this_year)) : 0}}</div>
                            <div class="dacl-total-name">ACTIVE INTERNET BANKERS</div>
                            <div class="trend">
                                {{$dacl && $dacl->b_trend == '-' ? '-' : '+'}}{{$dacl ? trend($dacl->bankers_last_year, $dacl->bankers_this_year) : 0}}
                                % TREND <span
                                        class="glyphicon glyphicon-arrow-{{$dacl && $dacl->b_trend == '-' ? 'down' : 'up'}}"></span>
                            </div>
                        </div>
                        <div class="dacl-box">
                            <div class="dacl-title">MOBILE ACTIVE DIGITAL CUSTOMERS</div>
                            <div class="gfx-container phones">
                                <div class="row">
                                    <div class="col-xs-6" style="">
                                        <div>
                                            {{$dacl ? numberAlias($dacl->mobile_last_year, true) : 0}}<br/>
                                            <img height="{{94 * ($dacl ? $dacl->mly_height : 1)}}"
                                                 src="http://avora.s3.amazonaws.com/slides/phone-lg.png"><br/>
                                            {{$now->copy()->subYear()->format('M Y')}}
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div>
                                            {{$dacl ? numberAlias($dacl->mobile_this_year, true) : 0}}<br/>
                                            <img height="{{94 * ($dacl ? $dacl->mty_height : 1)}}"
                                                 src="http://avora.s3.amazonaws.com/slides/phone-lg.png"><br/>
                                            {{$now->format('M Y')}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="dacl-total">{{$dacl ? strtoupper(numberAlias($dacl->mobile_this_year)) : 0}}</div>
                            <div class="dacl-total-name">TOTAL ACTIVE USERS</div>
                            <div class="trend">
                                {{$dacl && $dacl->m_trend == '-' ? '-' : '+'}}{{$dacl ? trend($dacl->mobile_last_year, $dacl->mobile_this_year) : 0}}
                                % TREND <span
                                        class="glyphicon glyphicon-arrow-{{$dacl && $dacl->m_trend == '-' ? 'down' : 'up'}}"></span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row pip-container">
                <div class="pip pip-active first"></div>
                <div class="pip second"></div>
                <div class="pip third"></div>
            </div>
        </div>

@stop