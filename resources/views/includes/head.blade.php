<div class="row title-bar">
    <div class="col-xs-12 insights">
        <img src="http://avora.s3.amazonaws.com/insights.png">
        @if(Auth::check())
            <a href="/auth/logout"><span title="Logout" class="glyphicon glyphicon-share pull-right"></span></a>
        @elseif(stristr(Route::getCurrentRoute()->getPath(), 'admin'))
            <a href="/"><span title="Back to statistics" class="glyphicon glyphicon-share pull-right"></span></a>
        @else
            <a href="/admin"><span title="Login to statistics manager" class="glyphicon glyphicon-cog pull-right"></span></a>
        @endif
    </div>
</div>
<div class="row title-bar-line"></div>