<div class="row text-center">
    <div class="nav-square orange">
        <img src="http://avora.s3.amazonaws.com/navigation/online-share-sales-selected.png">
    </div>
    <div class="nav-square orange-grey">
        <img src="http://avora.s3.amazonaws.com/navigation/customer-satisfaction.png">
    </div>
    <div class="nav-square blue">
        <img src="http://avora.s3.amazonaws.com/navigation/digitally-active.png">
    </div>
    <div class="nav-square white">
        <img src="http://avora.s3.amazonaws.com/navigation/mobile-bank.png">
    </div>
    <div class="nav-square lgt-blue">
        <img src="http://avora.s3.amazonaws.com/navigation/research-traffic.png">
    </div>
    <div class="nav-square grey">
        <img src="http://avora.s3.amazonaws.com/navigation/customer-contacts.png">
    </div>
</div>