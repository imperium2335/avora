@extends('layouts.admin')

@section('css')

@stop

@section('content')
    <?php $now = \Carbon\Carbon::now();?>
    <div class="panel panel-default panel-main">
        <div class="panel-heading">
            <h3 class="panel-title">Data management</h3>
        </div>
        <div class="panel-body">
            <form id="statistics_form" action="/admin/save-stats">
                <div id="statistics" class="row">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="last_year_total_customers">Total customers
                                        in {{$now->copy()->subYear()->format('M Y')}}</label>

                                    <div class="input-group">
                                        <input type="number" class="form-control" id="last_year_total_customers"
                                               name="last_year_total_customers"
                                               value="{{$data ? $data->total_customers_last_year : 0}}"
                                               placeholder="Total customers">

                                        <div id="last_year_total_customers_friendly" class="input-group-addon">
                                            {{numberAlias($data ? $data->total_customers_last_year : 0)}}</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="this_year_total_customers">Total customers
                                        in {{$now->format('M Y')}}</label>

                                    <div class="input-group">
                                        <input type="number" class="form-control" id="this_year_total_customers"
                                               name="this_year_total_customers"
                                               value="{{$data ? $data->total_customers_current_year : 0}}"
                                               placeholder="Total customers">

                                        <div id="this_year_total_customers_friendly" class="input-group-addon">
                                            {{numberAlias($data ? $data->total_customers_current_year : 0)}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="last_year_bankers">Active internet bankers
                                        in {{$now->copy()->subYear()->format('M Y')}}</label>

                                    <div class="input-group">
                                        <input type="number" class="form-control" id="last_year_bankers"
                                               name="last_year_bankers" value="{{$data ? $data->bankers_last_year : 0}}"
                                               placeholder="Total internet bankers">

                                        <div id="last_year_bankers_friendly" class="input-group-addon">
                                            {{numberAlias($data ? $data->bankers_last_year : 0)}}</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="this_year_bankers">Active internet bankers
                                        in {{$now->format('M Y')}}</label>

                                    <div class="input-group">
                                        <input type="number" class="form-control" id="this_year_bankers"
                                               name="this_year_bankers" value="{{$data ? $data->bankers_this_year : 0}}"
                                               placeholder="Total internet bankers">

                                        <div id="this_year_bankers_friendly" class="input-group-addon">
                                            {{numberAlias($data ? $data->bankers_this_year : 0)}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="last_year_mobile">Mobile active digital customers
                                        in {{$now->copy()->subYear()->format('M Y')}}</label>

                                    <div class="input-group">
                                        <input type="number" class="form-control" id="last_year_mobile"
                                               name="last_year_mobile" value="{{$data ? $data->mobile_last_year : 0}}"
                                               placeholder="Total mobile users">

                                        <div id="last_year_mobile_friendly" class="input-group-addon">
                                            {{numberAlias($data ? $data->mobile_last_year : 0)}}</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="this_year_mobile">Mobile active digital customers
                                        in {{$now->format('M Y')}}</label>

                                    <div class="input-group">
                                        <input type="number" class="form-control" id="this_year_mobile"
                                               name="this_year_mobile" value="{{$data ? $data->mobile_this_year : 0}}"
                                               placeholder="Total mobile users">

                                        <div id="this_year_mobile_friendly" class="input-group-addon">
                                            {{numberAlias($data ? $data->mobile_this_year : 0)}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="online_shares" class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="last_year_mobile">Online share</label>

                                    <div class="input-group">
                                        <span class="input-group-addon">{{$now->copy()->subYears(5)->format('M Y')}}</span>
                                        <input type="number" min="0" max="100" class="form-control percent" id="year_1" name="year_1"
                                               value="{{$data ? $data->year_1 : 0}}"
                                               placeholder="{{$now->copy()->subYears(5)->format('M Y')}}">

                                        <div id="year_1_friendly" class="input-group-addon">
                                            {{numberAlias($data ? $data->year_1 : 0, 0, 1)}}</div>
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">{{$now->copy()->subYears(4)->format('M Y')}}</span>
                                        <input type="number" min="0" max="100" class="form-control percent" id="year_2" name="year_2"
                                               value="{{$data ? $data->year_2 : 0}}"
                                               placeholder="{{$now->copy()->subYears(4)->format('M Y')}}">

                                        <div id="year_2_friendly" class="input-group-addon">
                                            {{numberAlias($data ? $data->year_2 : 0, 0, 1)}}</div>
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">{{$now->copy()->subYears(3)->format('M Y')}}</span>
                                        <input type="number" min="0" max="100" class="form-control percent" id="year_3" name="year_3"
                                               value="{{$data ? $data->year_3 : 0}}"
                                               placeholder="{{$now->copy()->subYears(3)->format('M Y')}}">

                                        <div id="year_3_friendly" class="input-group-addon">
                                            {{numberAlias($data ? $data->year_3 : 0, 0, 1)}}</div>
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">{{$now->copy()->subYears(2)->format('M Y')}}</span>
                                        <input type="number" min="0" max="100" class="form-control percent" id="year_4" name="year_4"
                                               value="{{$data ? $data->year_4 : 0}}"
                                               placeholder="{{$now->copy()->subYears(2)->format('M Y')}}">

                                        <div id="year_4_friendly" class="input-group-addon">
                                            {{numberAlias($data ? $data->year_4 : 0, 0, 1)}}</div>
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">{{$now->copy()->subYears(1)->format('M Y')}}</span>
                                        <input type="number" min="0" max="100" class="form-control percent" id="year_5" name="year_5"
                                               value="{{$data ? $data->year_5 : 0}}"
                                               placeholder="{{$now->copy()->subYears(1)->format('M Y')}}">

                                        <div id="year_5_friendly" class="input-group-addon">
                                            {{numberAlias($data ? $data->year_5 : 0, 0, 1)}}</div>
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">{{$now->format('M Y')}}</span>
                                        <input type="number" min="0" max="100" class="form-control percent" id="year_6" name="year_6"
                                               value="{{$data ? $data->year_6 : 0}}"
                                               placeholder="{{$now->format('M Y')}}">

                                        <div id="year_6_friendly" class="input-group-addon">
                                            {{numberAlias($data ? $data->year_6 : 0, 0, 1)}}</div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="panel-footer"><b>Everything is awesome!</b></div>
    </div>
@stop