@extends('layouts.admin')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/login.css">
@stop

@section('content')

    <div class="page">
        <div class="modal-dialog login">
            <div class="modal-content">
                <form method="post" action="auth/login">
                <div class="modal-header">
                    <h4 class="modal-title">Login</h4>
                </div>
                <div class="modal-body">

                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" value="{{Request::old('email')}}" name="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                        </div>
                    @if(count($errors) > 0)
                        <div class="alert alert-warning">
                            {{$errors->first()}}
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Login</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@stop